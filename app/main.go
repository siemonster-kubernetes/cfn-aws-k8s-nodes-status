package main

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/eks"
	v1 "k8s.io/api/core/v1"
	"log"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"sigs.k8s.io/aws-iam-authenticator/pkg/token"
)

func newClientset(cluster *eks.Cluster) (*kubernetes.Clientset, error) {
	// log.Printf("%+v", cluster)
	gen, err := token.NewGenerator(true, false)
	if err != nil {
		return nil, err
	}
	opts := &token.GetTokenOptions{
		ClusterID: aws.StringValue(cluster.Name),
	}
	tok, err := gen.GetWithOptions(opts)
	if err != nil {
		return nil, err
	}
	ca, err := base64.StdEncoding.DecodeString(aws.StringValue(cluster.CertificateAuthority.Data))
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfig(
		&rest.Config{
			Host:        aws.StringValue(cluster.Endpoint),
			BearerToken: tok.Token,
			TLSClientConfig: rest.TLSClientConfig{
				CAData: ca,
			},
		},
	)
	if err != nil {
		return nil, err
	}
	return clientset, nil
}

func getInstanceId(node *v1.Node) (string, error) {
	providerId := string(node.Spec.ProviderID)
	split := strings.Split(providerId, "/")
	if len(split) != 5 {
		return "", errors.New("got empty EC2 instance ID")
	}
	return split[4], nil
}

func genCloudWatchMetricData(clusterName string, nodes *v1.NodeList) ([]*cloudwatch.MetricDatum, error) {
	var data []*cloudwatch.MetricDatum
	data = append(data, &cloudwatch.MetricDatum{
		MetricName: aws.String("NodesCount"),
		Unit:       aws.String("Count"),
		Value:      aws.Float64(float64(len(nodes.Items))),
		Dimensions: []*cloudwatch.Dimension{
			&cloudwatch.Dimension{
				Name:  aws.String("ClusterName"),
				Value: aws.String(clusterName),
			},
		},
	})

	for _, node := range nodes.Items {
		ec2Id, err := getInstanceId(&node)
		if err != nil {
			return nil, err
		}
		// fmt.Printf("%s\n", ec2Id)
		for _, condition := range node.Status.Conditions {
			// fmt.Printf("\t%s: %s\n", condition.Type, condition.Status)
			bitSetVar := 0.0
			if strings.ToLower(string(condition.Status)) == strings.ToLower("True") {
				bitSetVar = 1.0
			}
			data = append(data, &cloudwatch.MetricDatum{
				MetricName: aws.String(string(condition.Type)),
				Unit:       aws.String("Count"),
				Value:      aws.Float64(bitSetVar),
				Dimensions: []*cloudwatch.Dimension{
					&cloudwatch.Dimension{
						Name:  aws.String("ClusterName"),
						Value: aws.String(clusterName),
					},
					&cloudwatch.Dimension{
						Name:  aws.String("InstanceId"),
						Value: aws.String(ec2Id),
					},
				},
			})
		}
	}
	return data, nil
}

func handleRequest(ctx context.Context, event events.CloudWatchEvent) (string, error) {
	name, ok := os.LookupEnv("CLUSTER_NAME")
	if !ok {
		log.Fatalf("Required environment variable is not found: CLUSTER_NAME")
	}
	region, ok := os.LookupEnv("AWS_REGION")
	if !ok {
		log.Fatalf("Required environment variable is not found: AWS_REGION")
	}
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(region),
	}))
	eksSvc := eks.New(sess)
	cwSvc := cloudwatch.New(sess)

	input := &eks.DescribeClusterInput{
		Name: aws.String(name),
	}
	result, err := eksSvc.DescribeCluster(input)
	if err != nil {
		log.Fatalf("Error calling DescribeCluster: %v", err)
	}
	clientset, err := newClientset(result.Cluster)
	if err != nil {
		log.Fatalf("Error creating clientset: %v", err)
	}
	nodes, err := clientset.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Fatalf("Error getting EKS nodes: %v", err)
	}
	log.Printf("There are %d nodes associated with cluster %s", len(nodes.Items), name)
	data, err := genCloudWatchMetricData(name, nodes)
	if err != nil {
		log.Fatalf("Error processing Kubernetes nodes: %v", err)
	}
	_, err = cwSvc.PutMetricData(&cloudwatch.PutMetricDataInput{
		Namespace:  aws.String("Kubernetes/Nodes"),
		MetricData: data,
	})
	if err != nil {
		log.Fatalf("Error adding metrics: %v", err.Error())
	}
	return fmt.Sprintf("OK"), nil
}

func main() {
	lambda.Start(handleRequest)
}
